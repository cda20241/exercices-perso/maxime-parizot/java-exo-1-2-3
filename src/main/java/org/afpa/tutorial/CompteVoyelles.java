package org.afpa.tutorial;

import java.text.Normalizer;
import java.util.Scanner;

public class CompteVoyelles {
    public static void main(String[] args) {
        /*Compte le nombre de voyelles dans une phrase saisie par l'utilisateur*/
        Scanner input = new Scanner(System.in); // Créer un Input pour la phrase
        String phrase = input.nextLine();
        phrase = phrase.toLowerCase();
        String phraseSansAccents = Normalizer.normalize(phrase, Normalizer.Form.NFD);
        int[] compteVoyelles = new int[6]; // Créer un tableau contenant les voyelles
        String voyelles = "aeiouy";
        for (int i = 0; i < phraseSansAccents.length(); i++) { // Parcourt la phrase à la recherche de voyelles
            char c = phraseSansAccents.charAt(i);
            if (voyelles.indexOf(c) != -1) { // Si une voyelle est trouvée, l'ajoute au compteur voyelles
                compteVoyelles[voyelles.indexOf(c)]++;
            }
        }
        System.out.println(); // Pas vraiment utile, mais c'est plus joli avec (print une ligne vide)
        for (int i = 0; i < 6; i++) {
            System.out.println(voyelles.charAt(i) + ": " + compteVoyelles[i]); // Donne le résultat du calcul
        }
    }
}