package org.afpa.tutorial;
import java.text.Normalizer;
import java.util.Scanner;

public class QuellePlace {
    public static void main(String[] args) {
        /* Vérifie si la lettre entrée par l'utilisateur est présente dans la phrase entrée par l'utilisateur,
         print sa positon*/
        Scanner input = new Scanner(System.in); // Créer un Input pour la phrase
        String phrase = input.nextLine();
        phrase = phrase.toLowerCase();
        String phraseSansAccents = Normalizer.normalize(phrase, Normalizer.Form.NFD);
        phraseSansAccents = phraseSansAccents.replaceAll("[^\\p{ASCII}]", "");
        char charactere = input.nextLine().charAt(0); // Input pour le charactere/lettre
        for (int i = 0 ; i<phraseSansAccents.length() ; i++)
            if (phraseSansAccents.charAt(i) == charactere)
                System.out.println("La lettre " + "'" + charactere + "'" + " est présent à la position " + (i+1));/*
                Retourne la position de la lettre sous la forme d'un print*/
    }
}