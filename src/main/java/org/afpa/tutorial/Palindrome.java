package org.afpa.tutorial;
import java.text.Normalizer;
import java.util.Scanner;
public class Palindrome {
    public static void main(String[] args) {
        /* Vérifie si la phrase saisie par l'utilisateur est un palindrome ou non*/
            Scanner input = new Scanner(System.in); // Créer un Input pour la phrase
            String phrase = input.nextLine();
            phrase = phrase.toLowerCase();
            String phraseSansAccents = Normalizer.normalize(phrase, Normalizer.Form.NFD);
            phraseSansAccents = phraseSansAccents.replaceAll("[^\\p{ASCII}]", "");
            String phraseInverse = new StringBuilder(phraseSansAccents).reverse().toString();
            if (phraseInverse.equals(phraseSansAccents)){ // Vérifie si la phrase est égale dans les deux sens
                System.out.println("Le mot " + phrase + " est un palindrome");}
            if (!phraseInverse.equals(phraseSansAccents)){
                System.out.println("Le mot " + phrase + " n'est pas un palindrome");}

    }
}
